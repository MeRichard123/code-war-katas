def is_solved(board):
    # Create the lists for cols and diagonals 
    # Unpack the row and map over the columns and make a 2d array
    boardCols = list(zip(*board))
    BoardDiagonals = getDiagonals(board)
    # Loop over the colums rows and diagonals
    for col in boardCols:
        for row in board:
            for diagonal in BoardDiagonals:
                # Check for sequence and return winner
                if all(i==2 for i in col) or all(i==2 for i in row) or all(i==2 for i in diagonal):
                    return 2
                if all(i==1 for i in col) or all(i==1 for i in row) or all(i==1 for i in diagonal):
                    return 1
                elif all(i==1 for i in diagonal):
                    return 1
    else:
        # If no winner check empty squares and return -1
        for row in board:
            if 0 in row:
                return -1
        else:
            return 0


# Map the diagonals to a 2d array
def getDiagonals(board):
    diaOne = []
    diaTwo = []
    counter = 2
    for i in range(0,3):
        diaOne.append(board[i][i])
        diaTwo.append(board[i][counter])
        counter -= 1
    diagonals = [diaOne,diaTwo]
    return diagonals


"""
If we were to set up a Tic-Tac-Toe game, we would want to know whether the board's current state is solved,
wouldn't we? Our goal is to create a function that will check that for us!

Assume that the board comes in the form of a 3x3 array, where the value is 0 if a spot is empty,
1 if it is an "X", or 2 if it is an "O", like so:

[[0, 0, 1],
 [0, 1, 2],
 [2, 1, 0]]

We want our function to return:

    -1 if the board is not yet finished (there are empty spots),
    1 if "X" won,
    2 if "O" won,
    0 if it's a cat's game (i.e. a draw).

You may assume that the board passed in is valid in the context of a game of Tic-Tac-Toe.
"""
