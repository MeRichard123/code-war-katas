import re

def to_camel_case(text):
    if text:
        textString = re.split("-|_",text)
        for i in range(len(textString)):
            if textString.index(textString[i]) != 0:
                textString[i] = textString[i].capitalize()
        textString = ''.join(textString)
        return textString
    else:
        return text

print(to_camel_case('the_Stealth_warrior'))
print(to_camel_case("The-Stealth-Warrior"))

