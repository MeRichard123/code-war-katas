def meeting(s):
    array = []
    # Split on ; and uppercase it. This makes an array which we can loop over.
    for person in s.upper().split(";"):
        # add this this to the array and format it on the brackets unpack into format
        array.append('({1}, {0})'.format(*person.split(":")))
    # return the sorted array
    return ''.join(sorted(array))
