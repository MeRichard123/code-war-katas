from string import ascii_lowercase
import re

def solve(s):
    # keep track of the values for each letter
    scores = []
    # Create a list of all the letters on the alphabet 
    alphabet = list(ascii_lowercase)
    # Generate a list of numbers from 1-26
    scoreVals = list(range(1,27))
    # Map the score to the letter
    scoreMap = dict(zip(alphabet,scoreVals))
    # Remove the vowels
    consonants = re.sub(r"a|e|i|o|u",'-',s).split("-")
    # Get rid of an left over empty strings
    consonants = list(filter(None,consonants))
    # Loop the consonants
    for cons in consonants:
        # if its a groups loop them individually
        if len(cons) > 1:
            total = 0
            for letter in list(cons):
                # Grab the score from the map
                total += scoreMap.get(letter)
            # add to scores
            scores.append(total)
        else:
            # If its a single letter get score and add
            scores.append(scoreMap.get(cons))
    # Return highest
    return max(scores)




print(solve("zodiacs"))

"""
Given a lowercase string that has alphabetic characters only and no spaces,
return the highest value of consonant substrings. Consonants are any letters of the alphabet except "aeiou".

We shall assign the following values: a = 1, b = 2, c = 3, .... z = 26.

For example, for the word "zodiacs", let's cross out the vowels. We get: "z o d ia cs"

-- The consonant substrings are: "z", "d" and "cs" and the values are z = 26, d = 4 and cs = 3 + 19 = 22. The highest is 26.
solve("zodiacs") = 26

For the word "strength", solve("strength") = 57
-- The consonant substrings are: "str" and "ngth" with values "str" = 19 + 20 + 18 = 57
and "ngth" = 14 + 7 + 20 + 8 = 49. The highest is 57.

"""
