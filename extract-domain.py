def domain_name(url):
    #return url
    if url[:5] == "https":
        domain = url.strip("https:")
        domain=domain[2:]
        if domain[:4] == "www.":
            domain = domain.strip("www.")
            end = domain.find(".")
            return domain[:end]
        else:
            end = domain.find(".")
            return domain[:end]
    elif url[:4] == "http":
        domain = url.strip("http:")
        domain = domain[2:]
        if domain[:4] == "www.":
            domain = domain.strip("www.")
            end = domain.find(".")
            return domain[:end]
        else:
            end = domain.find(".")
            return domain[:end]
    elif url[:3] == "www":
        domain = url.strip("www.")
        end = domain.find(".")
        return domain[:end]
    else:
        end = url.find(".")
        return url[:end]
