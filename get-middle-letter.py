def get_middle(s):
    middleIdx = len(s)//2
    if len(s) % 2 != 0:
        return s[middleIdx]
    else:
        return s[middleIdx-1] + s[middleIdx]
