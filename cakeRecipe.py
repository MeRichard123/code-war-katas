def cakes(recipe, available):
    cakeAmount = 0
    # Map over items to see if any thing is missing if so return 0 as you cant make a cake 
    # if something is missing.
    for key in recipe:
        if not key in available.keys():
            cakeAmount = 0
            return cakeAmount
        
    # Make a list for the amounts you can make per ingredient 
    ingredients = []
    # Map over the recipe
    for key in recipe.keys():
        # Get the vals foreach ingredient
        have = available.get(key)
        need = recipe.get(key)
        # Do a division to find how many are possible
        ingredients.append(have//need)
    # Return the lowest number because that's the max cakes
    cakeAmount = min(ingredients)
    return cakeAmount
               
               

recipe = {"apples": 3, "flour": 300, "sugar": 150, "milk": 100, "oil": 100}
available = {"sugar": 500, "flour": 2000, "milk": 2000}
print(cakes(recipe, available))

print(cakes({"flour": 500, "sugar": 200, "eggs": 1},
            {"flour": 1200, "sugar": 1200, "eggs": 5, "milk": 200}))

'''
Pete likes to bake some cakes. He has some recipes and ingredients.
Unfortunately he is not good in maths. Can you help him to find out,
how many cakes he could bake considering his recipes?

Write a function cakes(), which takes the recipe (object)
and the available ingredients (also an object) and returns the maximum
number of cakes Pete can bake (integer). For simplicity there are no
units for the amounts
(e.g. 1 lb of flour or 200 g of sugar are simply 1 or 200).
Ingredients that are not present in the objects, can be considered as 0.
'''
