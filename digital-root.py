def digital_root(n):
    #check if it has one digit yet
    while len(str(n)) > 1:
        #seperate it out
        vals = list(str(n))
        #set n to keep a running total
        n = 0
        #itterate over the list
        for i in vals:
            #add each number to the running total
            n += int(i)
    #return the total
    return n
