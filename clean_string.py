import re
def clean_string(s):
    listed = list(s)
    res = s
    # Get all occurances of the #
    indices = [i for i,e in enumerate(listed) if e == '#']
    # Loop over each occurance
    for i in indices:
        # remove 1 occurance of any character or none with '' 
        res = re.sub(r'(.|^)#','',res,1)
    return res
     
          


print(clean_string('abc#d##c')) #ac
print(clean_string('abc####d##c#')) # ''
print(clean_string("#######")) # ''
print(clean_string(""), "" ) # ''
print(clean_string("###^{6e.DA}####"))

'''
Assume "#" is like a backspace in string.
This means that string "a#bc#d" actually is "bd"

Your task is to process a string with "#" symbols.
'''
