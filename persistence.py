from functools import reduce

def persistence(n): 
    #amount for each muliplication
    amount = 0
    while len(str(n)) > 1: #check for length of answer
        seperated = list(str(n))    #seperate the string
        ints = []
        for i in seperated:     #makes each elem an int
            ints.append(int(i))
        n = (reduce(lambda x, y: x*y, ints))   #times each element together
        amount += 1   #increment after each multiplicating
    return amount
