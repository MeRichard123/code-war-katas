# https://www.codewars.com/kata/5208f99aee097e6552000148

def solution(s):
    # Remove the spaces
    st = ''.join(s.split(" "))
    # Keep track of all the upper case letters and spaces
    specials = []
    # Loop to populate the array based on the input string
    for i in list(st):
        if i.isupper():
            specials.append(i)
        elif i == " ":
            specials.append(i)
    # call the split function 
    return recursiveSplit(s,0,specials)


def recursiveSplit(s,idx,arr):
    # Base Case all split?
    if s.count(" ") == len(arr):
        return s
    # get position of the letter currently being split
    pos = list(s).index(arr[idx])
    # Check if you are splitting a letter and the char before is a space hence already split
    if arr[idx].isalpha() and s[pos-1] != " ":
        # split the string on the letter based on the current index and specails array
        s = s.split(arr[idx])
        # Rejoin with a space
        joinStr = f" {arr[idx]}"
        # Perfom the Join
        s = joinStr.join(s)
    # Recursive call to the function
    return recursiveSplit(s,idx+1,arr)
