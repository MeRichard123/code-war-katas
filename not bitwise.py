binary = list(input(':'))

# Option One

for i in range(len(binary)):
     if binary[i]=='1':binary[i]='0'
     else: binary[i]='1'
print(''.join(binary))

# Option Two

print(input(':').translate({48:49,49:48}))

# Option Three

print(':'.join(str(1-int(c))for c in input()))
