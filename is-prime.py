from math import sqrt

def is_prime(num):
    prime = True
    if num > 2:
        #Checking numbers from n to the root of n because
        #after then timestables loop and its unnecessary to 
        #check them all.
        for i in range(2,int(sqrt(num)+1)):
            #check for divisibility
            if (num % i) == 0:
                prime = False
                break
    #if its less than 2
    elif num < 2:
        prime = False
    #so if its 2 return true
    else:
        prime = True
    return prime
