# https://www.codewars.com/kata/55c45be3b2079eccff00010f

def order(sentence):
    # Dict: Sort by the number in the word mapped to the word if the sentence isn't ""
    words = {sorted(word)[0]:word for word in sentence.split(" ") if sentence !=""}
    # Sort by key (number) and join
    return ' '.join([words[position] for position in sorted(words)])

"""
def order(sentence):
    string = []
    words = {}
    for word in sentence.split(" "):
        if sentence != "":
            words[sorted(word)[0]] = word
    for position in sorted(words):
        string.append(words[position])
    return ' '.join(string)"""
