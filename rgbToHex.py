# https://www.codewars.com/kata/513e08acc600c94f01000001

def rgb(r, g, b):
    print(r,g,b)
    if r > 255: r = 255
    if g > 255: g = 255
    if b > 255: b = 255
    output = ""
    if r < 0:
        output += "00"
    else:
        if len(hex(r)[2:].upper()) == 1:output += ('0'+ hex(r)[2:].upper())
        else:output += hex(r)[2:].upper()
    
    if g < 0:
        output += "00"
    else:
        if len(hex(g)[2:].upper()) == 1:output += ('0' + hex(g)[2:].upper()) 
        else:output += hex(g)[2:].upper()
    
    if b < 0:
        output += "00"
    else:
        if len(hex(b)[2:].upper()) == 1:output += ('0'+ hex(b)[2:].upper()) 
        else:output += (hex(b)[2:].upper())
    return output
