# https://www.codewars.com/kata/5526fc09a1bbd946250002dc

def find_outlier(integers):
    # Split the numbers into 2 arrays one even one odd
    evens = [n for n in integers if n % 2 == 0]
    odds = [n for n in integers if n % 2 != 0]
    # return the array with the outlier
    if len(evens) == 1: return evens[0]
    elif len(odds) == 1: return odds[0]
